### Валидация поля через setValidationConfig

Прмиер валидации с помощью метода setValidationConfig.

```js
methods: {
    setValidationConfig: function() {
        this.callParent(arguments);
        this.addColumnValidator("Phone", function (value) {
            var invalidMessage = "";
            var phone = this.get("Phone");
            if (phone.length < 3) {
                invalidMessage = "Минимальная длина - 3 символа";
            }
            return {
                invalidMessage: invalidMessage
            };
        });
    },
},
