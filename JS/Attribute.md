### Атрибут

Пример атрибута.

```js
"Status": {
	"isRequired": true,
	lookupListConfig: {
		columns: ["Finish"]
	},
	dependencies: [
		{
			columns: ["Status"],
			methodName: "onStatusChanged"
		}
	]
},
