### Добавить кнопку «Добавить» к справочнику через SQL

Полная статья: https://customerfx.com/article/creating-an-edit-page-for-a-lookup-in-creatio/

```sql

DECLARE
-- Name of edit page schema
@CardSchemaName NCHAR(100) = 'UsrMyLookupEditPage',
-- Name of object schema
@EntitySchemaName NCHAR(100) = 'UsrMyLookup',
-- Caption of your page
@PageCaption NCHAR(100) = 'My lookup page'
 
insert into SysModuleEntity(
    ProcessListeners,
    SysEntitySchemaUId
)
values(
    0,
    (select TOP 1 UId
    from SysSchema
    where Name = @EntitySchemaName
    )
)
 
insert into SysModuleEdit(
    SysModuleEntityId,
    UseModuleDetails,
    Position,
    HelpContextId,
    ProcessListeners,
    CardSchemaUId,
    ActionKindCaption,
    ActionKindName,
    PageCaption
)
values (
    (select top 1 Id
    from SysModuleEntity
    where SysEntitySchemaUId = (
        select TOP 1 UId
        from SysSchema
        where Name = @EntitySchemaName
        )
    ),
    1,
    0,
    '',
    0,
    (select top 1 UId
     from SysSchema
     where Name = @CardSchemaName
    ),
    '',
    '',
    @PageCaption
)
 
insert into SysModuleEditLcz (RecordId, SysCultureId, ActionKindCaption, PageCaption)
values (
  (select top 1 sme.Id
        from SysModuleEdit as sme
            left join SysModuleEntity as sment on sment.Id = sme.SysModuleEntityId
            left join SysSchema as sse on sse.UId = sment.SysEntitySchemaUId
            left join SysSchema as ss on ss.UId = sme.CardSchemaUId
        where sse.Name = @EntitySchemaName
            and ss.Name = @CardSchemaName
        order by sme.CreatedOn desc
  ), 
  'A5420246-0A8E-E111-84A3-00155D054C03', 
  'Add', 
  @PageCaption
)
