# DevBook
Описание

## C#

- [Вызов бизнес-процесса](CSharp/CallBusinessProcess.md)
- [Макрос Word](CSharp/WordMacros.md)

## JS

- [Валидация setValidationConfig](JS/SetValidationConfig.md)
- [Пример атрибута](JS/Attribute.md)

## SQL

- [Добавить кнопку «Добавить» к справочнику](SQL/LookupAddButton.md)

## Другое
