### Макрос Word

Пример макроса Word.

```csharp
namespace Terrasoft.Configuration
{
    using System;
    using System.Web;
    using Terrasoft.Core;

    [ExpressionConverterAttribute("CurrentDate")]
    class CurrentDateConverter : IExpressionConverter
    {
        private UserConnection _userConnection;

        public string Evaluate(object value, string arguments = "")
        {
            try
            {
                _userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
                return _userConnection.CurrentUser.GetCurrentDateTime().Date.ToString("dd MMM yyyy");
            }
            catch (Exception err)
            {
                return err.Message;
            }
        }
    }
}

