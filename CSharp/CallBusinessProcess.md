### Вызов бизнес-процесса из кода C#

Как вызвать бизнес-процесс из кода на C#.

```csharp
var param = new Dictionary<string, string>();
param["To"] = contact.PrimaryColumnValue.ToString();
param["Opportunity"] = opportunity.PrimaryColumnValue.ToString();
param["OpportunityLink"] = opportunityLink;
UserConnection.ProcessEngine.ProcessExecutor.Execute("ProcessName", param);
